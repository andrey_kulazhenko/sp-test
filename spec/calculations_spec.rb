# frozen_string_literal: true

require 'rspec'
require 'spec_helper'
require_relative '../services/visits'
require_relative '../services/uniq_views'

describe 'Calculate' do
  context 'instances' do
    let(:visits) { Visits.new nil }
    let(:uniq) { UniqViews.new nil }

    it 'should have visits suffix' do
      expect(visits.suffix).to eq('visits')
    end

    it 'should have uniq views suffix' do
      expect(uniq.suffix).to eq('uniq views')
    end

    it 'should receive data on initialize' do
      data = {address: 'ips'}
      visits_with_data = Visits.new data
      expect(visits_with_data.data).to eq(data)

      uniq_with_data = UniqViews.new data
      expect(uniq_with_data.data).to eq(data)
    end

    it 'should have run method' do
      expect(visits).to respond_to(:run)
      expect(uniq).to respond_to(:run)
    end
  end

  context 'methods' do
    let!(:data) { { about: %w[192.168.1.3], index: %w[192.168.1.1 192.168.1.2 192.168.1.1] } }
    let!(:uniq_data) { { about: %w[192.168.1.3], index: %w[192.168.1.1 192.168.1.2] } }
    let(:visits) { Visits.new(data) }
    let(:uniq) { UniqViews.new(data) }

    it 'should prepare data' do
      visits.send(:prepare)
      expect(visits.data).to eq(data)
      uniq.send(:prepare)
      expect(uniq.data).to eq(uniq_data)
    end

    it 'should sort data' do
      visits.send(:sort)
      expect(visits.data.transpose[0].join(' ')).to eq('index about')
    end

    it 'should calculate data' do
      visits.send(:calculate)
      expect(visits.data).to eq(['about 1 visits', 'index 3 visits'])
    end
  end
end
