# frozen_string_literal: true

require 'rspec'
require 'spec_helper'
require_relative '../bin/parser'

describe 'Parser' do
  let(:log_file_path) { File.join(File.dirname(__FILE__), 'fixtures', 'correct_log_file.log') }
  let(:wrong_ext_log) { File.join(File.dirname(__FILE__), 'fixtures', 'incorrect_log_file.txt') }
  let(:wrong_content_log) { File.join(File.dirname(__FILE__), 'fixtures', 'wrong_log_content.log') }

  context 'when log file exists' do
    it 'should print page views ordered by visits descend' do
      expect { run log_file_path }.to output(%r{/contact 9 visits /about 8 visits /index 5 visits}).to_stdout
    end

    it 'should print uniq page views ordered by visits descend' do
      expect { run log_file_path }.to output(%r{/contact 5 uniq views /about 4 uniq views /index 3 uniq views}).to_stdout
    end
  end

  context 'when log file wrong' do
    it 'should print error message when log file does not exist' do
      expect { run 'wrong_file.log' }.to output("An error occurred, log file doesn't exists\n").to_stdout
    end

    it 'should print error message when log file has wrong extension' do
      expect { run wrong_ext_log }.to output("An error occurred, log file has wrong extension\n").to_stdout
    end

    it 'should print error message when wrong log file content' do
      expect { run wrong_content_log}.to output("An error occurred, wrong log content\n").to_stdout
    end
  end
end
