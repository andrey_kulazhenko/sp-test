# frozen_string_literal: true

require './services/calculate'

class UniqViews
  include Calculate

  def initialize(data)
    super
    @suffix = 'uniq views'
  end

  protected

  def prepare
    @data = @data.transform_values(&:uniq)
    self
  end
end
