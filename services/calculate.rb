# frozen_string_literal: true

module Calculate

  attr_reader :suffix, :data

  def initialize(data)
    @data = data
    @suffix = ''
  end

  def run
    prepare
    sort
    calculate
    @data.join(' ')
  end

  protected

  def prepare; end

  def sort
    @data = @data.sort_by { |_, v| -v.length }
  end

  def calculate
    @data = @data.map { |page, ips| "#{page} #{ips.length} #{@suffix}" }
  end
end