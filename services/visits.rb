# frozen_string_literal: true

require './services/calculate'

class Visits
  include Calculate

  def initialize(data)
    super
    @suffix = 'visits'
  end
end
