# frozen_string_literal: true

require './services/visits'
require './services/uniq_views'

class FileReader

  LINE_REGEX = %r{^/[\w/\-?=&]+ \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}}.freeze

  def initialize(log_file)
    @log_file = log_file
  end

  def process
    check_file
    data = read_data
    visits = Visits.new data
    puts visits.run
    uniq_views = UniqViews.new data
    puts uniq_views.run
  end

  private

  attr_reader :log_file

  def check_file
    raise "An error occurred, log file doesn't exists" unless File.exist? @log_file
    raise 'An error occurred, log file has wrong extension' unless File.extname(@log_file) == '.log'
  end

  def read_data
    log = File.read @log_file
    stat = {}
    log.each_line do |line|
      raise 'An error occurred, wrong log content' unless LINE_REGEX.match?(line)

      page, ip = line.chomp.split(/\s/)
      stat[page] ||= []
      stat[page] << ip
    end
    stat
  end
end
