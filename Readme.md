SP Tech Task
=============================
Write a ruby script that:

- Receives a log as argument (webserver.log is provided) e.g.: ./parser.rb webserver.log
- Returns the following:
    - list of webpages with most page views ordered from most pages views to less page views e.g.:
    
        `/home 90 visits /index 80 visits etc...` 
    
    - list of webpages with most unique page views also ordered e.g.:
    
        `/about/2 8 unique views /index 5 unique views etc...`

### REQUIREMENTS

Ruby 2.6.1

### QUICK START

- Install dependencies

```sh 
$ bundle install --path=.bundle
```

- Run script with command
```sh
$ bundle exec ruby bin/parser.rb webserver.log
```

### TESTS
The tests use the [RSpec framework](http://rspec.info/)

- Run the tests: 
```sh
$ bundle exec rspec
```