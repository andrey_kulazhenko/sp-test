#!/usr/bin/env ruby
require './services/file_reader'

def run(file)
  parser = FileReader.new file
  begin
    parser.process
  rescue StandardError => e
    puts e
  end
end

run ARGV.shift if $PROGRAM_NAME == __FILE__
